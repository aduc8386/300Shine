package com.aduc8386.app300shine.util
import android.widget.ImageView

interface ImageLoader {

    fun load(imageView: ImageView, url: String)

}