package com.aduc8386.app300shine.util

import android.widget.ImageView
import com.aduc8386.app300shine.R
import com.bumptech.glide.Glide
import javax.inject.Inject

class GlideImageLoader @Inject constructor() : ImageLoader {
    override fun load(imageView: ImageView, url: String) {
        Glide.with(imageView.context)
            .load(url)
            .centerCrop()
            .error(R.drawable.ic_app_logo)
            .into(imageView)
    }
}