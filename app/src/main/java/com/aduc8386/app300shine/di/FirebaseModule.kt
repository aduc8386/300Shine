package com.aduc8386.app300shine.di

import com.aduc8386.app300shine.ref.FirebaseReferences
import com.aduc8386.app300shine.data.datasource.BarbershopDatasource
import com.aduc8386.app300shine.data.datasource.CustomerDatasource
import com.aduc8386.app300shine.data.datasource.remote.BarbershopFirebaseDatasource
import com.aduc8386.app300shine.data.datasource.remote.CustomerFirebaseDatasource
import com.aduc8386.app300shine.data.repo.BarbershopFirebaseRepository
import com.aduc8386.app300shine.data.repo.CustomerFirebaseRepository
import com.aduc8386.app300shine.domain.repo.BarbershopRepository
import com.aduc8386.app300shine.domain.repo.CustomerRepository
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.ktx.Firebase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object FirebaseModule {

    @Provides
    @Singleton
    fun providesFirebaseAuth(): FirebaseAuth = Firebase.auth

    @Provides
    @Singleton
    fun providesFirebaseDatabase(): FirebaseDatabase = FirebaseDatabase.getInstance()

    @BarbershopDatabaseReferences
    @Provides
    @Singleton
    fun providesBarbershopFirebaseReference(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReferences.BARBERSHOP_REFERENCES)

    @CustomerDatabaseReferences
    @Provides
    @Singleton
    fun providesCustomerFirebaseReference(firebaseDatabase: FirebaseDatabase): DatabaseReference =
        firebaseDatabase.getReference(FirebaseReferences.CUSTOMER_REFERENCES)

    @Provides
    @Singleton
    fun providesBarbershopFirebaseDatasource(
        @BarbershopDatabaseReferences databaseReferences: DatabaseReference,
    ): BarbershopDatasource = BarbershopFirebaseDatasource(databaseReferences)

    @Provides
    @Singleton
    fun providesCustomerFirebaseDatasource(
        firebaseAuth: FirebaseAuth,
        @CustomerDatabaseReferences databaseReferences: DatabaseReference,
    ): CustomerDatasource = CustomerFirebaseDatasource(firebaseAuth, databaseReferences)

    @Provides
    @Singleton
    fun providesBarbershopFirebaseRepository(
        barbershopDatasource: BarbershopDatasource
    ): BarbershopRepository = BarbershopFirebaseRepository(barbershopDatasource)

    @Provides
    @Singleton
    fun providesCustomerFirebaseRepository(
        customerDatasource: CustomerDatasource
    ): CustomerRepository = CustomerFirebaseRepository(customerDatasource)

}