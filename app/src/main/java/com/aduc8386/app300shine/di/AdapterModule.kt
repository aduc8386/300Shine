package com.aduc8386.app300shine.di

import com.aduc8386.app300shine.presentation.component.barbershop.gallery.BarbershopGalleryAdapter
import com.aduc8386.app300shine.presentation.component.barbershop.review.BarbershopReviewAdapter
import com.aduc8386.app300shine.presentation.component.barbershop.service.BarbershopServiceAdapter
import com.aduc8386.app300shine.presentation.component.barbershop.barber.BarbershopBarberAdapter
import com.aduc8386.app300shine.presentation.component.booking.BarbershopBarberSpinnerAdapter
import com.aduc8386.app300shine.presentation.component.booking.BarbershopDateSpinnerAdapter
import com.aduc8386.app300shine.presentation.component.booking.BarbershopServiceSpinnerAdapter
import com.aduc8386.app300shine.presentation.component.booking.BookingTimeAdapter
import com.aduc8386.app300shine.presentation.component.home.BarbershopAdapter
import com.aduc8386.app300shine.presentation.component.mybooking.CurrentBookingAdapter
import com.aduc8386.app300shine.presentation.component.mybooking.HistoryBookingAdapter
import com.aduc8386.app300shine.util.ImageLoader
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent


@Module
@InstallIn(FragmentComponent::class)
object AdapterModule {

    @Provides
    fun providesBarbershopAdapter(imageLoader: ImageLoader): BarbershopAdapter = BarbershopAdapter(imageLoader)

    @Provides
    fun providesBarbershopServiceAdapter(): BarbershopServiceAdapter = BarbershopServiceAdapter()

    @Provides
    fun providesBarbershopReviewAdapter(): BarbershopReviewAdapter = BarbershopReviewAdapter()

    @Provides
    fun providesBarbershopBarberAdapter(): BarbershopBarberAdapter = BarbershopBarberAdapter()

    @Provides
    fun providesBarbershopServiceSpinnerAdapter(): BarbershopServiceSpinnerAdapter =
        BarbershopServiceSpinnerAdapter()

    @Provides
    fun providesBarbershopBarberSpinnerAdapter(): BarbershopBarberSpinnerAdapter =
        BarbershopBarberSpinnerAdapter()

    @Provides
    fun providesBarbershopDateSpinnerAdapter(): BarbershopDateSpinnerAdapter =
        BarbershopDateSpinnerAdapter()

    @Provides
    fun providesBarbershopGalleryAdapter(): BarbershopGalleryAdapter =
        BarbershopGalleryAdapter()

    @Provides
    fun providesBookingTimeAdapter(): BookingTimeAdapter =
        BookingTimeAdapter()

    @Provides
    fun providesCurrentBookingAdapter(): CurrentBookingAdapter =
        CurrentBookingAdapter()

    @Provides
    fun providesHistoryBookingAdapter(): HistoryBookingAdapter =
        HistoryBookingAdapter()

}