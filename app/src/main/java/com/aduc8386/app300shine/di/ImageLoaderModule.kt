package com.aduc8386.app300shine.di

import com.aduc8386.app300shine.util.GlideImageLoader
import com.aduc8386.app300shine.util.ImageLoader
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class ImageLoaderModule {

    @Binds
    abstract fun bindsGlideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader

}