package com.aduc8386.app300shine.di

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class BarbershopDatabaseReferences

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class CustomerDatabaseReferences