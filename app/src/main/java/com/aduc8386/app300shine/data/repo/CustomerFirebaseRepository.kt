package com.aduc8386.app300shine.data.repo

import com.aduc8386.app300shine.data.datasource.CustomerDatasource
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.Customer
import com.aduc8386.app300shine.domain.repo.CustomerRepository
import com.aduc8386.app300shine.presentation.component.CustomerState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class CustomerFirebaseRepository @Inject constructor(
    private val customerDatasource: CustomerDatasource
) : CustomerRepository {
    override suspend fun insertCustomerFullInformation(customer: Customer): CustomerState =
        customerDatasource.insertCustomerFullInformation(customer)

    override suspend fun insertCustomerEmailNamePhonePassword(customer: Customer): CustomerState =
        customerDatasource.insertCustomerEmailNamePhonePassword(customer)

    override suspend fun getCustomerByEmailAndPassword(email: String, password: String): CustomerState =
        customerDatasource.getCustomerByEmailAndPassword(email, password)

    override suspend fun getCurrentCustomer(): Flow<CustomerState> =
        customerDatasource.getCurrentCustomer()

    override fun insertBooking(booking: Booking): Boolean = customerDatasource.insertBooking(booking)
}