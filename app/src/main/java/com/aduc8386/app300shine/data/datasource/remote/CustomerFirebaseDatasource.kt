package com.aduc8386.app300shine.data.datasource.remote

import android.util.Log
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.datasource.CustomerDatasource
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.Customer
import com.aduc8386.app300shine.di.CustomerDatabaseReferences
import com.aduc8386.app300shine.presentation.component.CustomerState
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class CustomerFirebaseDatasource @Inject constructor(
    private val firebaseAuth: FirebaseAuth,
    @CustomerDatabaseReferences private val customerDatabaseReferences: DatabaseReference
) : CustomerDatasource {

    override suspend fun insertCustomerFullInformation(customer: Customer): CustomerState {
        return CustomerState.Error("No implemented")
    }

    override suspend fun insertCustomerEmailNamePhonePassword(customer: Customer): CustomerState {
        return try {
            firebaseAuth.createUserWithEmailAndPassword(
                customer.email,
                customer.password
            ).await()

            if (firebaseAuth.currentUser != null) {
                val registeredUser =
                    Customer(
                        id = firebaseAuth.currentUser!!.uid,
                        email = customer.email,
                        name = customer.name,
                        phoneNumber = customer.phoneNumber,
                        password = customer.password
                    )
                customerDatabaseReferences.child(registeredUser.id).setValue(registeredUser).await()
                CustomerState.Registered(registeredUser)
            } else {
                CustomerState.Error("Something wrong happened")
            }
        } catch (exception: FirebaseException) {
            CustomerState.Error("Something wrong happened")
        }
    }

    override suspend fun getCustomerByEmailAndPassword(
        email: String,
        password: String
    ): CustomerState {
        return try {
            firebaseAuth.signInWithEmailAndPassword(email, password).await()

            val customer = customerDatabaseReferences.child(firebaseAuth.currentUser!!.uid).get()
                .await().getValue(Customer::class.java)

            customer?.let { CustomerState.LoggedIn(it) } ?: CustomerState.Error("User not founded")
        } catch (exception: FirebaseException) {
            CustomerState.Error("Something wrong happened")
        }
    }

    override suspend fun getCurrentCustomerInformation(): Flow<Customer?> = callbackFlow {
        if (firebaseAuth.currentUser == null) {
            trySend(null)
        }

        val currentCustomerReference = firebaseAuth.currentUser?.uid?.let {
            customerDatabaseReferences.child(
                it
            )
        }

        val listener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val customerLogin = dataSnapshot.getValue(Customer::class.java)

                trySend(
                    customerLogin
                )
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        }

        currentCustomerReference?.addValueEventListener(listener)

        awaitClose {
            currentCustomerReference?.removeEventListener(listener)
        }
    }

    override suspend fun getCurrentCustomer(): Flow<CustomerState> =
        getCurrentCustomerInformation().combine(getBookings()) { customer, bs ->
            customer?.let {
                customer.apply {
                    bookings = bs as MutableList<Booking>
                }
                CustomerState.LoggedIn(customer)
            } ?: CustomerState.Error("User not founded")
        }

    override fun insertBooking(booking: Booking): Boolean {

        if (firebaseAuth.currentUser == null) {
            return false
        }

        val currentCustomerBookingReference = firebaseAuth.currentUser?.uid?.let { userId ->
            customerDatabaseReferences.child(
                "$userId/bookings"
            )
        }

        var k = ""

        currentCustomerBookingReference?.push()?.apply {
            booking.apply {
                k = key!!
                id = key!!
            }
            setValue(booking)
        }

        return true
    }

    override suspend fun getBookings(): Flow<List<Booking>> = callbackFlow {
        val currentCustomerBookingReference = firebaseAuth.currentUser?.uid?.let { userId ->
            customerDatabaseReferences.child(
                "$userId/bookings"
            )
        }

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val bookings = mutableListOf<Booking>()

                for (workLogging in snapshot.children) {
                    workLogging.getValue(Booking::class.java)?.let {
                        bookings.add(it)
                    }
                }

                trySend(bookings)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("ducna", "onCancelled: called")
            }
        }


        currentCustomerBookingReference?.addValueEventListener(listener)
        awaitClose { currentCustomerBookingReference?.removeEventListener(listener) }
    }
}