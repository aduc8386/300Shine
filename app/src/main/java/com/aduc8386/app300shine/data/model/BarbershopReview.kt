package com.aduc8386.app300shine.data.model

import java.util.*
import kotlin.random.Random

data class BarbershopReview(
    val id: String,
    val customerName: String,
    val updateTime: Long,
    val likes: Int,
    val rating: Double,
    val customerProfile: String,
    val customerType: String,
    val content: String
) {
//    companion object {
//        fun random(barbershopId: String): BarbershopReview {
//            val randomIndex = Random.nextInt(0, 1000)
//            return BarbershopReview(
//                id = UUID.randomUUID().toString(),
//                barbershopId = barbershopId,
//                customerName = "USER $randomIndex",
//                likes = Random.nextInt(0, 100),
//                customerProfile = "https://www.richardtmoore.co.uk/wp-content/uploads/2016/10/btx-avatar-placeholder-01-2.jpg",
//                updateTime = System.currentTimeMillis(),
//                content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
//                rating = String.format(Locale.US, "%.2f", Random.nextDouble(0.0, 5.0)).toDouble(),
//                customerType = "Regular Customer"
//            )
//        }
//    }
}
