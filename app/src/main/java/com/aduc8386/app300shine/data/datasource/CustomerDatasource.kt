package com.aduc8386.app300shine.data.datasource

import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.Customer
import com.aduc8386.app300shine.presentation.component.CustomerState
import kotlinx.coroutines.flow.Flow

interface CustomerDatasource {

    suspend fun insertCustomerFullInformation(customer: Customer): CustomerState

    suspend fun insertCustomerEmailNamePhonePassword(
        customer: Customer
    ): CustomerState

    suspend fun getCustomerByEmailAndPassword(email: String, password: String): CustomerState

    suspend fun getCurrentCustomerInformation(): Flow<Customer?>

    suspend fun getCurrentCustomer(): Flow<CustomerState>

    fun insertBooking(booking: Booking): Boolean

    suspend fun getBookings(): Flow<List<Booking>>

}