package com.aduc8386.app300shine.data.model

import com.google.firebase.database.Exclude
import java.util.*
import kotlin.random.Random

data class Customer(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val email: String = "",
    val password: String = "",
    val phoneNumber: String = "",
    val dateOfBirth: String = "",
    val address: String = "",
    val type: String = "Normal customer",

    @Exclude
    @get:Exclude
    @set:Exclude
    var bookings: MutableList<Booking> = mutableListOf()
) {
    companion object {
        fun random(): Customer {
            val randomIndex = Random.nextInt(0, 1000)
            return Customer(
                id = UUID.randomUUID().toString(),
                name = "CUSTOMER $randomIndex",
                password = "CUSTOMER $randomIndex",
                phoneNumber = Random.nextLong(1_000_000_000, 9_999_999_999).toInt().toString(),
                dateOfBirth = "${Random.nextInt(1,12)}/${Random.nextInt(1, 31)}/${Random.nextInt(1990, 2023)}",
                address = randomIndex.toString(),
                type = "Regular Customer"
            )
        }
    }
}