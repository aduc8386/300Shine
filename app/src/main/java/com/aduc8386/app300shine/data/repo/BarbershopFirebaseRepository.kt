package com.aduc8386.app300shine.data.repo

import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.datasource.BarbershopDatasource
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.domain.repo.BarbershopRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BarbershopFirebaseRepository @Inject constructor(
    private val barbershopDatasource: BarbershopDatasource
) : BarbershopRepository {
    override suspend fun getBarbershops(): Flow<List<Barbershop>> =
        barbershopDatasource.getBarbershops()

    override suspend fun getBarbershopById(id: String): ResponseStatus<Barbershop> = barbershopDatasource.getBarbershopById(id)

    override suspend fun getBarbershopsByName(query: String): Flow<List<Barbershop>> = barbershopDatasource.getBarbershopsByName(query)
}