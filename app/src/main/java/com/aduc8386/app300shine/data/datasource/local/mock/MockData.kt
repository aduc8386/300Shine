package com.aduc8386.app300shine.data.datasource.local.mock

import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.data.model.BarbershopBarber
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.data.model.ServiceByBarber
import java.util.UUID

object MockData {

    val barbers = listOf(
        BarbershopBarber(
            name = "Nguyễn Anh Đức",
            rating = 4.5,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "ducna@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Đỗ Thị Thu Hương",
            rating = 4.5,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "huongdtt@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Nguyễn Hoàng Anh",
            rating = 4.1,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "anhhn@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Trịnh Xuân Đào",
            rating = 4.2,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "daotx@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Đỗ Ngọc Ánh",
            rating = 4.4,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "anhdn@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Lù Thị Thu Hằng",
            rating = 4.1,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "hangltt@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Phạm Thị Việt Anh",
            rating = 4.1,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "anhptv@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Nguyễn Đức Mạnh",
            rating = 4.4,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "manhnd@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Phạm Quang Minh",
            rating = 4.5,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "minhpq@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Nguyễn Văn Nhật",
            rating = 4.4,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "nhatnv@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Bùi Thị Thảo",
            rating = 4.3,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "thaobt@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Nguyễn Thị Ngọc Hà",
            rating = 4.0,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "hantn@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Bùi Thị Tươi",
            rating = 4.0,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "tuoibt@gmail.com",
            gender = "female"
        ),
        BarbershopBarber(
            name = "Nguyễn Công Trình",
            rating = 4.0,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "trinhnc@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Nguyễn Minh Hiếu",
            rating = 4.6,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "hieunm@gmail.com",
            gender = "male"
        ),
        BarbershopBarber(
            name = "Nguyễn Vũ Minh",
            rating = 4.3,
            profile = "https://i.pravatar.cc/300?u=${UUID.randomUUID()}",
            phoneNumber = "",
            email = "minhnv@gmail.com",
            gender = "male"
        ),
    )

    val barbershops = listOf(
        Barbershop(
            name = "30Shine",
            profile = "https://timxungquanh.com/UserFile/timxungquanh/CompanyImage/073c8dee-d1b2-4a7c-9ae7-42e2c655d8e8.jpg",
            banner = "https://nqtm.vn/wp-content/uploads/2020/07/cua-hang-30-shine.jpg",
            rating = 4.0,
            phoneNumber = "0359 368 111",
            email = "30shine@gmail.com",
            address = "702 Đường Láng, Láng Thượng",
            services = listOf(
                BarbershopService(
                    name = "Cắt tóc",
                    price = 100_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 30.0,
                    rating = 4.1,
                    picture = "https://cdn.diemnhangroup.com/seoulacademy/hoc-cat-toc-nam-1.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[0]
                        ),
                        ServiceByBarber(
                            barber = barbers[1]
                        ),
                        ServiceByBarber(
                            barber = barbers[2]
                        ),
                    )
                ),
                BarbershopService(
                    name = "Nhuộm tóc",
                    price = 200_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.4,
                    picture = "https://inhat.vn/wp-content/uploads/2021/06/cat-toc-nam-bien-hoa-3-min.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[1]
                        ),
                        ServiceByBarber(
                            barber = barbers[2]
                        ),
                    )
                ),
                BarbershopService(
                    name = "Uốn tóc",
                    price = 200_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.1,
                    picture = "https://topsalon.vn/wp-content/uploads/2019/11/cat-toc-o-30shine-5.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[0]
                        ),
                    )
                ),
                BarbershopService(
                    name = "Massage",
                    price = 50_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 20.0,
                    rating = 4.8,
                    picture = "https://i.imgur.com/jwWyEfi.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[0]
                        ),
                        ServiceByBarber(
                            barber = barbers[1]
                        ),
                        ServiceByBarber(
                            barber = barbers[2]
                        ),
                        ServiceByBarber(
                            barber = barbers[3]
                        )
                    )
                ),
            ),
            barbers = listOf(
                barbers[0],
                barbers[1],
                barbers[2],
                barbers[3],
            )
        ),
        Barbershop(
            name = "Zuy Minh",
            profile = "https://file.hstatic.net/1000172157/article/_nh_ti_u_d__blog_16jpg_1024x1024.jpg",
            banner = "https://i.ytimg.com/vi/6U2C2_6ZpV8/maxresdefault.jpg",
            rating = 4.5,
            phoneNumber = "0359 368 222",
            email = "zuyminh@gmail.com",
            address = "103 B4 Trung Tự, Phạm Ngọc Thạch",
            services = listOf(
                BarbershopService(
                    name = "Cắt tóc",
                    price = 150_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 50.0,
                    rating = 4.5,
                    picture = "https://i.ytimg.com/vi/KRdUbS_7C20/maxresdefault.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[4],
                        ),
                        ServiceByBarber(
                            barber = barbers[5],
                        ),
                        ServiceByBarber(
                            barber = barbers[6],
                        ),
                        ServiceByBarber(
                            barber = barbers[7],
                        ),
                        ServiceByBarber(
                            barber = barbers[8],
                        ),
                    )
                ),
                BarbershopService(
                    name = "Nhuộm tóc",
                    price = 250_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 90.0,
                    rating = 4.7,
                    picture = "https://file.hstatic.net/1000172157/file/zuy_minh_salon_-_2019_-_tsb02_03_-_anh_09_9acc15fafb5548a9891976227f9d0d4f_grande.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[6],
                        ),
                        ServiceByBarber(
                            barber = barbers[7],
                        ),
                        ServiceByBarber(
                            barber = barbers[8],
                        ),
                    )
                ),
                BarbershopService(
                    name = "Uốn tóc",
                    price = 250_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 90.0,
                    rating = 4.3,
                    picture = "https://file.hstatic.net/1000172157/article/olaplex___mm_-_clip_ae_viet_nam_-_zminh___viet_-_anh_bia_clip_01_cda7d96ef39c4de59f826d1328b4f80b_1024x1024.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[4],
                        ),
                        ServiceByBarber(
                            barber = barbers[5],
                        ),
                        ServiceByBarber(
                            barber = barbers[6],
                        ),
                    )
                ),
            ),
            barbers = listOf(
                barbers[4],
                barbers[5],
                barbers[6],
                barbers[7],
                barbers[8],
            )
        ),
        Barbershop(
            name = "IronCap Barbershop",
            profile = "https://ironcapbarbershop.com/wp-content/uploads/2019/09/logo.png",
            banner = "https://newsmd2fr.keeng.net/tiin/archive/imageslead/2023/02/27/90_5b6709fbb1a0b9a40179cd5ed620a4c0.png",
            rating = 4.8,
            phoneNumber = "0359 368 333",
            email = "ironcap@gmail.com",
            address = "16 Ngõ 189, Nguyễn Văn Cừ, Long Biên",
            services = listOf(
                BarbershopService(
                    name = "Cắt tóc",
                    price = 150_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 40.0,
                    rating = 4.6,
                    picture = "https://ss-images.saostar.vn/wpr700/pc/1677225548830/saostar-bewhhagpfzszr2ml.png",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[9],
                        ),
                        ServiceByBarber(
                            barber = barbers[10],
                        ),
                        ServiceByBarber(
                            barber = barbers[11],
                        )
                    )
                ),
                BarbershopService(
                    name = "Nhuộm tóc",
                    price = 250_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.5,
                    picture = "https://toplist.vn/images/800px/ironcap-barbershop-996533.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[9],
                        ),
                        ServiceByBarber(
                            barber = barbers[10],
                        ),
                        ServiceByBarber(
                            barber = barbers[11],
                        )
                    )
                ),
                BarbershopService(
                    name = "Uốn tóc",
                    price = 250_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.4,
                    picture = "https://toplist.vn/images/800px/ironcap-barbershop-1013950.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[9],
                        ),
                        ServiceByBarber(
                            barber = barbers[10],
                        ),
                        ServiceByBarber(
                            barber = barbers[11],
                        )
                    )
                ),
            ),
            barbers = listOf(
                barbers[9],
                barbers[10],
                barbers[11]
            )
        ),
        Barbershop(
            name = "4Rau Barbershop",
            profile = "https://images2.thanhnien.vn/Uploaded/thaoltd/2021_09_29/5-4rau-barber-shop_UVSO.jpg",
            banner = "https://thebureauasia.com/wp-content/uploads/2018/01/4rau-barber-1.jpg",
            rating = 4.6,
            phoneNumber = "0359 368 444",
            email = "4rau@gmail.com",
            address = "77 Yersin, Phường Cầu Ông Lãnh, Quận 1",
            services = listOf(
                BarbershopService(
                    name = "Cắt tóc",
                    price = 200_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 40.0,
                    rating = 4.2,
                    picture = "https://file1.hutech.edu.vn/file/editor/homepage1/1550abcf8b187a462309.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[12],
                        ),
                        ServiceByBarber(
                            barber = barbers[13],
                        ),
                        ServiceByBarber(
                            barber = barbers[14],
                        ),
                        ServiceByBarber(
                            barber = barbers[15],
                        )
                    )
                ),
                BarbershopService(
                    name = "Nhuộm tóc",
                    price = 350_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.3,
                    picture = "https://toplist.vn/images/800px/ironcap-barbershop-996533.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[13],
                        ),
                        ServiceByBarber(
                            barber = barbers[14],
                        ),
                        ServiceByBarber(
                            barber = barbers[15],
                        )
                    )
                ),
                BarbershopService(
                    name = "Uốn tóc",
                    price = 350_000.0,
                    description = "Stylist chuyên nghiệp sẽ trao đổi thật kỹ để thấu hiểu và chọn ra kiểu tóc tuyệt vời nhất với gương mặt và phong cách sống của bạn. Từ những lần sau, bạn chỉ cần ngả lưng và thư giãn giống như hàng ngàn thành viên quen thuộc khác. Stylist sẽ hiểu chính xác và cho bạn đúng kiểu tóc mong muốn.",
                    duration = 60.0,
                    rating = 4.4,
                    picture = "https://kenh14cdn.com/thumb_w/660/2017/image003-1500986315085.jpg",
                    serviceByBarbers = listOf(
                        ServiceByBarber(
                            barber = barbers[12],
                        ),
                        ServiceByBarber(
                            barber = barbers[13],
                        ),
                        ServiceByBarber(
                            barber = barbers[14],
                        ),
                    )
                ),
            ),
            barbers = listOf(
                barbers[12],
                barbers[13],
                barbers[14],
                barbers[15],
            )
        )
    )
}