package com.aduc8386.app300shine.data.model

import java.util.Locale
import java.util.UUID
import kotlin.random.Random

data class Barbershop(
    var id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val profile: String = "",
    val banner: String = "",
    val email: String = "",
    val phoneNumber: String = "",
    val address: String = "",
    val rating: Double = 0.0,

    val services: List<BarbershopService> = listOf(),
    val gallery: List<String> = listOf(
        "https://www.beyoung.in/blog/wp-content/uploads/2020/04/general-min-1-853x1024.jpg",
        "https://i.pinimg.com/originals/26/03/1b/26031b6495eff75b1336777cbd78098d.jpg",
        "https://www.menshairstyletrends.com/wp-content/uploads/2020/11/Skin-fade-haircut-z_ramsey-1024x1024.jpg",
        "https://i.pinimg.com/474x/64/5e/39/645e395eccbc53324f32c5372fa6e036.jpg",
        "https://i.pinimg.com/736x/b4/3c/49/b43c499e3dda476b395539f69303a05d.jpg",
        "https://media6.ppl-media.com/mediafiles/blogs/image_c6b56fafbd.png",
        "https://i.pinimg.com/736x/f6/a9/cf/f6a9cff61e65a034fce9b54420cc6ff5.jpg",
        "https://i.pinimg.com/736x/a2/bf/52/a2bf52707464da36691392084aca6dd0.jpg",
        "https://i.pinimg.com/originals/fc/14/f2/fc14f2e4903b043daf8ad6638ec56e4d.jpg",
    ),
    val barbers: List<BarbershopBarber> = listOf(),
    val reviews: List<BarbershopReview> = listOf(),

    ) {
    companion object {
        fun random(): Barbershop {
            val randomIndex = Random.nextInt(0, 1000)
            return Barbershop(
                id = UUID.randomUUID().toString(),
                name = "BARBERSHOP $randomIndex",
                profile = "https://www.headzupbarbershop.com/wp-content/uploads/2020/08/Logo-of-Headz-Up-Barber-Shop-Miami-Lakes-scaled.jpg",
                banner = "https://heygoldie.com/blog/wp-content/uploads/2021/12/barber-shop-decor-ideas.jpg",
                email = "barbershop$randomIndex@gmail.com",
                phoneNumber = Random.nextLong(1_000_000_000, 9_999_999_999).toInt().toString(),
                address = "$randomIndex",
                rating = String.format(Locale.US, "%.2f", Random.nextDouble(0.0, 5.0)).toDouble()
            )
        }
    }
}
