package com.aduc8386.app300shine.data

sealed class ResponseStatus<T>(
    var data: T? = null,
    val msg: String? = null
) {

    class Success<T>(data: T, msg: String? = null) : ResponseStatus<T>(data = data, msg = msg)

    class Error<T>(msg: String, data: T? = null) : ResponseStatus<T>(data = data, msg = msg)

    class Loading<T>() : ResponseStatus<T>()

}