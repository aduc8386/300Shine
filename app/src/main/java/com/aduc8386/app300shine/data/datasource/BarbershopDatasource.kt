package com.aduc8386.app300shine.data.datasource

import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import kotlinx.coroutines.flow.Flow

interface BarbershopDatasource {

    suspend fun getBarbershops(): Flow<List<Barbershop>>

    suspend fun getBarbershopById(id: String): ResponseStatus<Barbershop>

    suspend fun getBarbershopsByName(query: String): Flow<List<Barbershop>>

}