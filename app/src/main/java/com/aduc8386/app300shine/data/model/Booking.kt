package com.aduc8386.app300shine.data.model

import java.util.UUID

data class Booking(
    var id: String = UUID.randomUUID().toString(),
    val bookingTime: String = "",
    val bookingDate: String = "",
    val status: String = "In process",
    val checkInTime: Long? = null,
    val checkOutTime: Long? = null,

    val barbershop: Barbershop? = null,
    val serviceByBarber: ServiceByBarber? = null,
    val service: BarbershopService? = null
)
