package com.aduc8386.app300shine.data.model

import java.util.*
import kotlin.random.Random

data class BarbershopBarber(
    val id: String = UUID.randomUUID().toString(),
    val profile: String = "",
    val rating: Double = 0.0,
    val name: String = "",
    val email: String = "",
    val phoneNumber: String = "",
    val gender: String = "",
    val type: String = ""
) {
    companion object {
        fun random(): BarbershopBarber {
            val randomIndex = Random.nextInt(0, 1000)
            return BarbershopBarber(
                id = UUID.randomUUID().toString(),
                name = "Barber $randomIndex",
                type = "Barbershop",
                profile = "https://www.headzupbarbershop.com/wp-content/uploads/2020/08/Logo-of-Headz-Up-Barber-Shop-Miami-Lakes-scaled.jpg",
                rating = String.format(Locale.US, "%.2f", Random.nextDouble(0.0, 5.0)).toDouble(),
                email = "barber$randomIndex@gmail.com",
                phoneNumber = Random.nextLong(1_000_000_000, 9_999_999_999).toInt().toString(),
                gender = listOf("Male", "Female").random(),
            )
        }
    }
}
