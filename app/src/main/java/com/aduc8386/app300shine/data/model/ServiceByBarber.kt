package com.aduc8386.app300shine.data.model

import java.util.Locale
import java.util.UUID
import kotlin.random.Random

data class ServiceByBarber(
    val id: String = UUID.randomUUID().toString(),

    val barber: BarbershopBarber? = null,
) {
    companion object {
        fun random(): ServiceByBarber {
            return ServiceByBarber(
                id = UUID.randomUUID().toString(),
            )
        }
    }
}
