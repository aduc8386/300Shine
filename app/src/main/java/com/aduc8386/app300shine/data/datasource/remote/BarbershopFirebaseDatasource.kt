package com.aduc8386.app300shine.data.datasource.remote

import android.util.Log
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.datasource.BarbershopDatasource
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.di.BarbershopDatabaseReferences
import com.google.firebase.FirebaseException
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class BarbershopFirebaseDatasource @Inject constructor(
    @BarbershopDatabaseReferences private val barbershopReference: DatabaseReference
) : BarbershopDatasource {

    override suspend fun getBarbershops(): Flow<List<Barbershop>> = callbackFlow {

//        MockData.barbershops.forEach {barbershop ->
//            barbershopReference.push().apply {
//                val idKey = key
//                barbershop.apply {
//                    id = idKey!!
//                }
//                setValue(barbershop)
//            }
//        }

        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val barbershops = mutableListOf<Barbershop>()
                snapshot.children.forEach {
                    it.getValue(Barbershop::class.java)?.let { barbershop ->
                        barbershops.add(barbershop)
                    }
                }
                trySend(barbershops)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("ducna", "onCancelled: getBarbershops cancelled")
                trySend(listOf())
            }
        }

        barbershopReference.addValueEventListener(listener)
        awaitClose {
            barbershopReference.removeEventListener(listener)
        }
    }

    override suspend fun getBarbershopById(id: String): ResponseStatus<Barbershop> {
        return try {

            val barbershop = barbershopReference.child(id).get()
                .await().getValue(Barbershop::class.java)

            Log.d("ducna", "getBarbershopById: $barbershop")
            if (barbershop != null) ResponseStatus.Success(barbershop) else ResponseStatus.Error("Null user")

        } catch (exception: FirebaseException) {
            ResponseStatus.Error(exception.message!!)
        }
    }

    override suspend fun getBarbershopsByName(query: String): Flow<List<Barbershop>> = callbackFlow {
        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val barbershops = mutableListOf<Barbershop>()
                snapshot.children.forEach {
                    it.getValue(Barbershop::class.java)?.let { barbershop ->
                        if (barbershop.name.lowercase().contains(query)) {
                            barbershops.add(barbershop)
                        }
                    }
                }
                trySend(barbershops)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.d("ducna", "onCancelled: getBarbershops cancelled")
                trySend(listOf())
            }
        }

        barbershopReference.addValueEventListener(listener)
        awaitClose {
            barbershopReference.removeEventListener(listener)
        }
    }
}