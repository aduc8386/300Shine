package com.aduc8386.app300shine.data.model

import java.util.*
import kotlin.collections.List
import kotlin.random.Random

data class BarbershopService(
    val id: String = UUID.randomUUID().toString(),
    val name: String = "",
    val price: Double = 0.0,
    val duration: Double = 0.0,
    val description: String = "",
    val picture: String = "",
    val rating: Double = 0.0,

    val serviceByBarbers: List<ServiceByBarber> = listOf()
) {
    companion object {
        fun random(): BarbershopService {
            val randomIndex = Random.nextInt(0, 1000)
            return BarbershopService(
                id = UUID.randomUUID().toString(),
                name = "SERVICE $randomIndex",
                price = String.format(Locale.US, "%.2f", Random.nextDouble(0.0, 20.0)).toDouble(),
                duration = String.format(Locale.US, "%.2f", Random.nextDouble(0.0, 2.0)).toDouble(),
                picture = "https://www.richardtmoore.co.uk/wp-content/uploads/2016/10/btx-avatar-placeholder-01-2.jpg",
                description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua",
            )
        }
    }
}