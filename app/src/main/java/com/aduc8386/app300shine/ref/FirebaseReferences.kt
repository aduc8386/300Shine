package com.aduc8386.app300shine.ref

object FirebaseReferences {

    const val BARBERSHOP_REFERENCES = "barbershops"
    const val CUSTOMER_REFERENCES = "customers"
    const val BARBERSHOP_OWNER_REFERENCES = "barbershop_owners"

}