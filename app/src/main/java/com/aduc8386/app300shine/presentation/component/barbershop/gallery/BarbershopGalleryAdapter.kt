package com.aduc8386.app300shine.presentation.component.barbershop.gallery

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.databinding.ItemBarbershopBarberBinding
import com.aduc8386.app300shine.databinding.ItemBarbershopGalleryBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.bumptech.glide.Glide

class BarbershopGalleryAdapter: BaseAdapter<ItemBarbershopGalleryBinding, String>() {
    override fun bindViewHolder(binding: ItemBarbershopGalleryBinding, item: String) {
        Glide.with(binding.root)
            .load(item)
            .centerCrop()
            .error(R.drawable.ic_about_us)
            .into(binding.rivItemBarbershopGalleryPicture)
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemBarbershopGalleryBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemBarbershopGalleryBinding.inflate(layoutInflater, viewGroup, false)
        }
}