package com.aduc8386.app300shine.presentation.component.profile

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.aduc8386.app300shine.databinding.FragmentProfileBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.CustomerState
import com.aduc8386.app300shine.presentation.component.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModels() {
        lifecycleScope.launch {
            launch {
                mainViewModel.currentCustomer.collectLatest {
                    bindCustomerInformation(it)
                }
            }
        }
    }

    private fun bindCustomerInformation(customerState: CustomerState) {
        if (customerState is CustomerState.LoggedIn) {
            with(binding) {
                tvProfileName.text = customerState.data!!.name
                tvProfileEmail.text = customerState.data.email
            }
        }
    }

    override fun bindView() {
        with(binding) {
            tvProfileNotification.setOnClickListener {
                Navigation.findNavController(root)
                    .navigate(ProfileFragmentDirections.actionProfileFragmentToProfileNotificationFragment())
            }
            tvProfileEmail.text
        }
    }
}