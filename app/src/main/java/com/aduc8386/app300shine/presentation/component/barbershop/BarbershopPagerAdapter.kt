package com.aduc8386.app300shine.presentation.component.barbershop

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aduc8386.app300shine.presentation.component.barbershop.gallery.BarbershopGalleryFragment
import com.aduc8386.app300shine.presentation.component.barbershop.review.BarbershopReviewFragment
import com.aduc8386.app300shine.presentation.component.barbershop.service.BarbershopServiceFragment
import com.aduc8386.app300shine.presentation.component.barbershop.barber.BarbershopBarberFragment

class BarbershopPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private var barbershopId: String = ""

    override fun getItemCount(): Int = 4

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> BarbershopServiceFragment().apply {
                arguments = bundleOf("BARBERSHOP_ID" to barbershopId)
            }
            1 -> BarbershopBarberFragment().apply {
                arguments = bundleOf("BARBERSHOP_ID" to barbershopId)
            }
            2 -> BarbershopGalleryFragment().apply {
                arguments = bundleOf("BARBERSHOP_ID" to barbershopId)
            }
            else -> BarbershopReviewFragment().apply {
                arguments = bundleOf("BARBERSHOP_ID" to barbershopId)
            }
        }
    }

    fun setBarbershopId(id: String) {
        barbershopId = id
    }
}