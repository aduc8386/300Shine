package com.aduc8386.app300shine.presentation.component.barbershop.gallery

import android.content.Context
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.databinding.FragmentBarbershopGalleryBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.barbershop.BarbershopViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class BarbershopGalleryFragment :
    BaseFragment<FragmentBarbershopGalleryBinding>(FragmentBarbershopGalleryBinding::inflate) {

    private val barbershopViewModel: BarbershopViewModel by viewModels()

    @Inject
    lateinit var barbershopGalleryAdapter: BarbershopGalleryAdapter

    private var barbershopId: String = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)

        barbershopId = requireArguments().getString("BARBERSHOP_ID", "null")
        Log.d("ducna", "onAttach: $barbershopId")
    }

    override fun bindView() {
        barbershopViewModel.getBarbershopInformation(barbershopId)
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            barbershopViewModel.barbershop.collectLatest {
                handleBarbershopInformation(it)
            }
        }
    }

    private fun handleBarbershopInformation(barbershopResponseStatus: ResponseStatus<Barbershop>?) {
        when (barbershopResponseStatus) {
            is ResponseStatus.Success -> {
                with(binding) {
                    barbershopGalleryAdapter.apply {
                        updateList(barbershopResponseStatus.data!!.gallery)
                    }
                    rcvBarbershopGallery.run {
                        layoutManager = GridLayoutManager(requireContext(), 3)
                        adapter = barbershopGalleryAdapter
                    }
                }
            }

            is ResponseStatus.Error -> {

            }

            is ResponseStatus.Loading -> {

            }

            else -> {

            }
        }
    }
}