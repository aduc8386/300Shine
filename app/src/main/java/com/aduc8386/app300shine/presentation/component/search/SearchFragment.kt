package com.aduc8386.app300shine.presentation.component.search

import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.databinding.FragmentSearchBinding
import com.aduc8386.app300shine.presentation.component.home.BarbershopAdapter
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.barbershop.BarbershopViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : BaseFragment<FragmentSearchBinding>(FragmentSearchBinding::inflate) {

    @Inject
    lateinit var barbershopAdapter: BarbershopAdapter

    private val barbershopViewModel: BarbershopViewModel by viewModels()

    override fun bindView() {

    }

    override fun observeViewModels() {

        val flow = callbackFlow {
            val listener = object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    trySend(s.toString())
                }

                override fun afterTextChanged(s: Editable?) {

                }
            }

            binding.edtSearchSearch.addTextChangedListener(listener)

            awaitClose {
                binding.edtSearchSearch.removeTextChangedListener(listener)
            }

        }.flowOn(Dispatchers.IO)

        lifecycleScope.launch {
            launch {
                repeatOnLifecycle(Lifecycle.State.STARTED) {
                    barbershopViewModel.searchBarbershops(
                        flow
                    )
                }
            }
            launch {
                barbershopViewModel.barbershops.collectLatest {
                    it?.run {
                        handleBarbershopsResponse(it)
                    }
                }
            }
        }
    }

    private fun handleBarbershopsResponse(barbershops: List<Barbershop>) {
        with(binding) {
            rcvSearchBarbershops.run {
                barbershopAdapter.apply {
                    updateList(barbershops)
                    setOnItemClickListener {
                        findNavController().navigate(
                            SearchFragmentDirections.actionGlobalBarbershopFragment(
                                it.id
                            )
                        )
                    }
                }
                layoutManager = LinearLayoutManager(requireContext())
                adapter = barbershopAdapter
            }
        }
    }
}