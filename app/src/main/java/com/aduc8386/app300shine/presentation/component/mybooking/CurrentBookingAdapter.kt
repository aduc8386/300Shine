package com.aduc8386.app300shine.presentation.component.mybooking

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.databinding.ItemCurrentBookingBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.bumptech.glide.Glide

class CurrentBookingAdapter : BaseAdapter<ItemCurrentBookingBinding, Booking>() {
    override fun bindViewHolder(binding: ItemCurrentBookingBinding, item: Booking) {
        with(binding) {
            Glide.with(root)
                .load(item.barbershop!!.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemCurrentBookingBarbershopProfile)

            Glide.with(root)
                .load(item.barbershop.banner)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemCurrentBookingBarbershopBanner)

            Glide.with(root)
                .load(item.serviceByBarber!!.barber!!.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(civItemCurrentBookingBarberProfile)

            tvItemCurrentBookingBarberName.text = item.serviceByBarber.barber!!.name
            tvItemCurrentBookingTime.text = "${item.bookingTime} ${item.bookingDate}"
            tvItemCurrentBookingName.text = item.barbershop.name
            tvItemCurrentBookingServiceName.text = item.service!!.name
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemCurrentBookingBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemCurrentBookingBinding.inflate(layoutInflater, viewGroup, false)
        }
}