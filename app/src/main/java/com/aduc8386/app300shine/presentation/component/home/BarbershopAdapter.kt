package com.aduc8386.app300shine.presentation.component.home

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.databinding.ItemBarbershopBinding
import com.aduc8386.app300shine.databinding.ItemBarbershopReviewBinding
import com.aduc8386.app300shine.databinding.ItemBarbershopServiceBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.aduc8386.app300shine.util.ImageLoader
import com.bumptech.glide.Glide
import javax.inject.Inject

class BarbershopAdapter @Inject constructor(
    private val imageLoader: ImageLoader
) : BaseAdapter<ItemBarbershopBinding, Barbershop>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemBarbershopBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemBarbershopBinding.inflate(layoutInflater, viewGroup, false)
        }

    override fun bindViewHolder(binding: ItemBarbershopBinding, item: Barbershop) {
        with(binding) {

            tvItemBarbershopName.text = item.name
            tvItemBarbershopAddress.text = item.address
            tvItemBarbershopPhoneNumber.text = item.phoneNumber
            tvItemBarbershopRating.text = item.rating.toString()
            rbItemBarbershopRate.rating = (item.rating / 5f).toFloat()

            Glide.with(root)
                .load(item.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemBarbershopProfile)

            imageLoader.load(rivItemBarbershopBanner, item.banner)

        }
    }
}