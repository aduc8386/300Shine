package com.aduc8386.app300shine.presentation.component.barbershop.service

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.databinding.ItemBarbershopServiceBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import java.text.DecimalFormat

class BarbershopServiceAdapter() :
    BaseAdapter<ItemBarbershopServiceBinding, BarbershopService>() {

    override fun bindViewHolder(binding: ItemBarbershopServiceBinding, item: BarbershopService) {
        with(binding) {

            val detail = "🕒 ${String.format("%.1f", item.duration / 60f)} hours ・ ${DecimalFormat("#,###").format(item.price)}đ"
            tvItemServiceName.text = item.name
            tvItemServiceDetail.text = detail
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemBarbershopServiceBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemBarbershopServiceBinding.inflate(layoutInflater, viewGroup, false)
        }
}