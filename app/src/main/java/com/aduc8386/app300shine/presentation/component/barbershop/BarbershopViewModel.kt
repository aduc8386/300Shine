package com.aduc8386.app300shine.presentation.component.barbershop

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.ServiceByBarber
import com.aduc8386.app300shine.domain.repo.BarbershopRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class BarbershopViewModel @Inject constructor(
    private val barbershopRepository: BarbershopRepository
) : ViewModel() {

    val dates by lazy {
        var currentDate = System.currentTimeMillis()

        val dates = mutableListOf<String>()
        repeat(7) {
            val dateString = SimpleDateFormat("EEE, MMM dd", Locale.US).format(
                currentDate
            )
            dates.add(dateString)
            currentDate += 24 * 60 * 60 * 1000
        }
        dates
    }

    private var _barbershop =
        MutableStateFlow<ResponseStatus<Barbershop>?>(null)
    val barbershop get() = _barbershop.asStateFlow()

    private var _barbershops =
        MutableStateFlow<List<Barbershop>?>(null)
    val barbershops get() = _barbershops.asStateFlow()

    private var _selectedService =
        MutableStateFlow<BarbershopService?>(null)
    val selectedService get() = _selectedService.asStateFlow()

    private var _selectedServiceByBarber =
        MutableStateFlow<ServiceByBarber?>(null)
    val selectedServiceByBarber get() = _selectedServiceByBarber.asStateFlow()

    private var _selectedDate =
        MutableStateFlow<String?>(null)
    val selectedDate get() = _selectedDate.asStateFlow()

    private var _selectedTime =
        MutableStateFlow<String?>(null)
    val selectedTime get() = _selectedTime.asStateFlow()

    private var _booking =
        MutableStateFlow(
            Booking(
                barbershop = _barbershop.value?.data,
                service = _barbershop.value?.data?.services?.get(0),
                bookingDate = dates[0]
            )
        )
    val booking = _booking.asStateFlow()

    fun getBarbershopInformation(barbershopId: String) {
        viewModelScope.launch {
            _barbershop.update { barbershopRepository.getBarbershopById(barbershopId) }
            _booking.update {
                _booking.value.copy(
                    barbershop = _barbershop.value!!.data
                )
            }
        }
    }

    fun setSelectedService(barbershopService: BarbershopService?) {
        viewModelScope.launch {
            _booking.update {
                _booking.value.copy(
                    service = barbershopService
                )
            }
            _selectedService.update { barbershopService }
            Log.d("ducna", "setSelectedService: ${_booking.value.service?.name}")
        }
    }

    fun setSelectedServiceByBarber(serviceByBarber: ServiceByBarber?) {
        viewModelScope.launch {
            _booking.update {
                _booking.value.copy(
                    serviceByBarber = serviceByBarber
                )
            }
            _selectedServiceByBarber.update { serviceByBarber }
            Log.d("ducna", "setSelectedServiceByBarber: ${_booking.value.serviceByBarber?.barber?.name}")
        }
    }

    fun setSelectedDate(date: String?) {
        viewModelScope.launch {
            _booking.update {
                _booking.value.copy(
                    bookingDate = date!!
                )
            }
            _selectedDate.update { date }
            Log.d("ducna", "setSelectedDate: ${_booking.value.bookingDate}")
        }
    }

    fun setSelectedTime(time: String?) {
        viewModelScope.launch {
            _booking.update {
                _booking.value.copy(
                    bookingTime = time!!
                )
            }
            _selectedTime.update { time }
            Log.d("ducna", "setSelectedTime: ${_booking.value.bookingTime}")
        }
    }

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    fun searchBarbershops(searchFlow: Flow<String>) {
        viewModelScope.launch {
            searchFlow
                .debounce(300)
                .filter { queryString ->
                    queryString.isNotEmpty()
                }
                .distinctUntilChanged()
                .flatMapLatest {
                    barbershopRepository.getBarbershopsByName(it)
                }.flowOn(Dispatchers.IO)
                .collect {
                    _barbershops.value = it
                    Log.d("ducna", "searchBarbershops: $it")
                }
        }
    }


}