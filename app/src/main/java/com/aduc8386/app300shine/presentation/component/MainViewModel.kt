package com.aduc8386.app300shine.presentation.component

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.Customer
import com.aduc8386.app300shine.domain.repo.CustomerRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val customerRepository: CustomerRepository
) : ViewModel() {

    private var _currentCustomer =
        MutableStateFlow<CustomerState>(CustomerState.LoggedOut(""))
    val currentCustomer get() = _currentCustomer.asStateFlow()

    fun getCurrentCustomer() {
        viewModelScope.launch {
            customerRepository.getCurrentCustomer().collectLatest {
                _currentCustomer.value = it
            }
        }
    }

    fun login(email: String, password: String) {
        viewModelScope.launch {
            _currentCustomer.update {
                customerRepository.getCustomerByEmailAndPassword(
                    email,
                    password
                )
            }
        }
    }

    fun register(email: String, password: String, name: String, phoneNumber: String) {
        viewModelScope.launch {
            val customer = Customer(
                email = email,
                password = password,
                name = name,
                phoneNumber = phoneNumber
            )
            _currentCustomer.update {
                customerRepository.insertCustomerEmailNamePhonePassword(
                    customer
                )
            }
        }
    }

    fun book(booking: Booking) {
        viewModelScope.launch {
            if (customerRepository.insertBooking(booking)) {
                customerRepository.getCurrentCustomer().collectLatest {
                    _currentCustomer.value = it
                }
            }
        }
    }
}