package com.aduc8386.app300shine.presentation.component.barbershop

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.databinding.FragmentBarbershopBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.MainActivity
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class BarbershopFragment :
    BaseFragment<FragmentBarbershopBinding>(FragmentBarbershopBinding::inflate) {

    private val args: BarbershopFragmentArgs by navArgs()

    private val barbershopViewModel: BarbershopViewModel by viewModels()

    override fun bindView() {

        (requireActivity() as MainActivity).hideBottomNavigation()

        Log.d("ducna", "bindView: ${args.barbershopId}")
        setViewPagerAdapter()

        barbershopViewModel.getBarbershopInformation(barbershopId = args.barbershopId)

        with(binding) {
            btnBarbershopBook.setOnClickListener(this@BarbershopFragment)
        }
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            barbershopViewModel.barbershop.collectLatest {
                handleBarbershopInformation(it)
            }
        }
    }

    private fun handleBarbershopInformation(barbershopResponseStatus: ResponseStatus<Barbershop>?) {
        when (barbershopResponseStatus) {
            is ResponseStatus.Success -> {
                bindBarbershopInformation(barbershopResponseStatus.data!!)
            }
            is ResponseStatus.Error -> {

            }
            is ResponseStatus.Loading -> {

            }
            else -> {

            }
        }
    }

    private fun setViewPagerAdapter() {
        binding.vpViewpager.apply {
            adapter = BarbershopPagerAdapter(this@BarbershopFragment).apply {
                setBarbershopId(args.barbershopId)
            }
        }
        TabLayoutMediator(binding.tlTabLayout, binding.vpViewpager) { tab, position ->
            tab.text = listOf("Services", "Specialists", "Gallery", "Reviews")[position]
        }.attach()

        for (i in 0..3) {
            val textView = LayoutInflater.from(requireContext())
                .inflate(R.layout.tab_layout_title, null) as TextView

            binding.tlTabLayout.getTabAt(i)?.customView = textView
        }
    }

    private fun bindBarbershopInformation(barbershop: Barbershop) {
        with(binding) {
            Glide.with(root)
                .load(barbershop.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivBarbershopProfile)

            Glide.with(root)
                .load(barbershop.banner)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivBarbershopBanner)

            tvBarbershopAddress.text = barbershop.address
            tvBarbershopName.text = barbershop.name
            rbBarbershopRate.rating = (barbershop.rating / 5).toFloat()
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            binding.btnBarbershopBook -> {
                findNavController().navigate(
                    BarbershopFragmentDirections.actionBarbershopFragmentToBookingFragment(
                        args.barbershopId
                    )
                )
            }
        }
    }
}