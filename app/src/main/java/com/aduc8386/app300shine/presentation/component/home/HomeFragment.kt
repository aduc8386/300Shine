package com.aduc8386.app300shine.presentation.component.home

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.databinding.FragmentHomeBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private val homeViewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var barbershopAdapter: BarbershopAdapter

    override fun bindView() {
        homeViewModel.getBarbershops()
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            homeViewModel.barbershops.collectLatest {
                handleBarbershopsResponse(it)
            }
        }
    }

    override fun onClick(v: View?) {

    }

    private fun handleBarbershopsResponse(barbershops: List<Barbershop>) {
        with(binding) {
            rcvHomeBarbershops.run {
                barbershopAdapter.apply {
                    updateList(barbershops)
                    setOnItemClickListener {
                        findNavController().navigate(
                            HomeFragmentDirections.actionGlobalBarbershopFragment(
                                it.id
                            )
                        )
                    }
                }
                layoutManager = LinearLayoutManager(requireContext())
                adapter = barbershopAdapter
            }
        }
    }
}