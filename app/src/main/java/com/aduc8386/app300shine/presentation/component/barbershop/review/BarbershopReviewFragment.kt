package com.aduc8386.app300shine.presentation.component.barbershop.review

import android.content.Context
import android.util.Log
import com.aduc8386.app300shine.databinding.FragmentBarbershopReviewBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class BarbershopReviewFragment :
    BaseFragment<FragmentBarbershopReviewBinding>(FragmentBarbershopReviewBinding::inflate) {


    @Inject
    lateinit var barbershopReviewAdapter: BarbershopReviewAdapter

    private var barbershopId: String = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)

        barbershopId = requireArguments().getString("BARBERSHOP_ID", "null")
        Log.d("ducna", "onAttach: $barbershopId")
    }

    override fun bindView() {
    }

    override fun observeViewModels() {
    }


}