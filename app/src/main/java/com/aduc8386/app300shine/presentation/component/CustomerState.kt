package com.aduc8386.app300shine.presentation.component

import com.aduc8386.app300shine.data.model.Customer

sealed class CustomerState(val data: Customer?, val msg: String?) {

    class LoggedIn(data: Customer) : CustomerState(data = data, msg = null)

    class Registered(data: Customer) : CustomerState(data = data, msg = null)

    class LoggedOut(msg: String) : CustomerState(data = null, msg = msg)

    class Error(msg: String) : CustomerState(data = null, msg = msg)

}
