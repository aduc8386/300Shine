package com.aduc8386.app300shine.presentation.component

import android.view.View
import androidx.activity.viewModels
import androidx.navigation.ui.setupWithNavController
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.databinding.ActivityMainBinding
import com.aduc8386.app300shine.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>(ActivityMainBinding::inflate) {

    private val mainViewModel: MainViewModel by viewModels()

    override fun bindView() {
        binding.bnvBottomNavigation.apply {
            navController.addOnDestinationChangedListener { _, destination, _ ->
                when(destination.id) {
                    R.id.homeFragment -> showBottomNavigation()
                    R.id.profileFragment -> showBottomNavigation()
                    R.id.searchFragment -> showBottomNavigation()
                    R.id.myBookFragment -> showBottomNavigation()
                    else -> hideBottomNavigation()
                }
            }
            setupWithNavController(navController)
        }


    }

    private fun showBottomNavigation() {
        binding.bnvBottomNavigation.visibility = View.VISIBLE
    }

    fun hideBottomNavigation() {
        binding.bnvBottomNavigation.visibility = View.GONE
    }

    fun showLoading() {
        binding.llcLoading.visibility = View.VISIBLE
        binding.root.isEnabled = false

    }

    fun hideLoading() {
        binding.llcLoading.visibility = View.GONE
        binding.root.isEnabled = true
    }
}