package com.aduc8386.app300shine.presentation.component.booking

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.databinding.ItemBookingTimeBinding

class BookingTimeAdapter : RecyclerView.Adapter<BookingTimeAdapter.BookingTimeHolder>() {

    private var itemSelected = ""

    private var onItemClickListener: ((item: String) -> Unit)? = null

    private var times = listOf<String>()

    fun setList(t: List<String>) {
        times = t
    }

    inner class BookingTimeHolder(val binding: ItemBookingTimeBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(time: String) {
            if (itemSelected == time) {
                binding.root.background = AppCompatResources.getDrawable(
                    binding.root.context,
                    R.drawable.bg_rounded_solid_pizazz
                )
            } else {
                binding.root.background = AppCompatResources.getDrawable(
                    binding.root.context,
                    R.drawable.bg_rounded_solid_clay
                )
            }
            binding.tvSpinnerTime.text = time
        }

        fun setSelect(time: String) {
            itemSelected = time
            notifyDataSetChanged()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookingTimeHolder {
        val binding =
            ItemBookingTimeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BookingTimeHolder(binding)
    }

    override fun onBindViewHolder(holder: BookingTimeHolder, position: Int) {
        val time = times[position]

        holder.bind(time)
        holder.binding.root.setOnClickListener {
            holder.setSelect(time)
            onItemClickListener?.let { it1 -> it1(time) }
        }
    }

    override fun getItemCount(): Int = times.size

    fun setOnItemClickListener(listener: (item: String) -> Unit){
        onItemClickListener = listener
    }
}