package com.aduc8386.app300shine.presentation.component.profile

import com.aduc8386.app300shine.databinding.FragmentProfileNotificationBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment

class ProfileNotificationFragment: BaseFragment<FragmentProfileNotificationBinding>(FragmentProfileNotificationBinding::inflate) {
    override fun bindView() {

    }
}