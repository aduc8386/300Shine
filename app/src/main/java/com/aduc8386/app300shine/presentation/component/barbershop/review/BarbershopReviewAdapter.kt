package com.aduc8386.app300shine.presentation.component.barbershop.review

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.BarbershopReview
import com.aduc8386.app300shine.databinding.ItemBarbershopReviewBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.bumptech.glide.Glide

class BarbershopReviewAdapter() : BaseAdapter<ItemBarbershopReviewBinding, BarbershopReview>() {

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemBarbershopReviewBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemBarbershopReviewBinding.inflate(layoutInflater, viewGroup, false)
        }

    override fun bindViewHolder(binding: ItemBarbershopReviewBinding, item: BarbershopReview) {
        with(binding) {
            Glide.with(root)
                .load(item.customerProfile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemBarbershopReviewUserProfile)

            tvItemBarbershopReviewUserName.text = item.customerName
            tvItemBarbershopReviewContent.text = item.content
            tvItemBarbershopReviewUserType.text = item.customerType
            rbItemBarbershopReviewRating.rating = item.rating.toFloat()
            tvItemBarbershopBarbershopReviewLikes.text = item.likes.toString()
        }
    }

}