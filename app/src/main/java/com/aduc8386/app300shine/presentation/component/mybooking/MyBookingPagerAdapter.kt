package com.aduc8386.app300shine.presentation.component.mybooking

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.aduc8386.app300shine.presentation.component.barbershop.barber.BarbershopBarberFragment

class MyBookingPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> CurrentBookingFragment()
            else -> HistoryBookingFragment()
        }
    }
}