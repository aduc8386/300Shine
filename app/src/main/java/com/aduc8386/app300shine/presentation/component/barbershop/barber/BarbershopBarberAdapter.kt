package com.aduc8386.app300shine.presentation.component.barbershop.barber

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.BarbershopBarber
import com.aduc8386.app300shine.databinding.ItemBarbershopBarberBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.bumptech.glide.Glide

class BarbershopBarberAdapter : BaseAdapter<ItemBarbershopBarberBinding, BarbershopBarber>() {
    override fun bindViewHolder(binding: ItemBarbershopBarberBinding, item: BarbershopBarber) {
        with(binding) {
            Glide.with(root)
                .load(item.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemBarbershopBarberProfile)

            tvItemBarbershopBarberName.text = item.name
            rbItemBarbershopBarberRate.rating = item.rating.toFloat()
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemBarbershopBarberBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemBarbershopBarberBinding.inflate(layoutInflater, viewGroup, false)
        }
}