package com.aduc8386.app300shine.presentation.component.booking

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.databinding.ItemSpinnerServiceBinding
import com.bumptech.glide.Glide
import java.text.DecimalFormat

class BarbershopServiceSpinnerAdapter : BaseAdapter() {
    //    override fun bindViewHolder(binding: ItemSpinnerServiceBinding, item: BarbershopService) {
//        with(binding) {
//            val duration = String.format("%.2f minutes", item.duration)
//            tvSpinnerServiceName.text = item.name
//            tvSpinnerServiceDuration.text = duration
//
//        }
//    }
//
//    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemSpinnerServiceBinding
//        get() = { layoutInflater, viewGroup, _ ->
//            ItemSpinnerServiceBinding.inflate(layoutInflater, viewGroup, false)
//        }

    private var services: List<BarbershopService> = listOf()

    override fun getCount(): Int = services.size

    override fun getItem(position: Int): Any = services[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding =
            ItemSpinnerServiceBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        val service = services[position]

        with(binding) {
            val duration = String.format("%.1f minutes", service.duration)
            tvSpinnerServiceName.text = service.name
            tvSpinnerServiceDuration.text = duration
            tvSpinnerServicePrice.text = "${DecimalFormat("#,###").format(service.price)}đ"
            Glide.with(root)
                .load(service.picture)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivSpinnerServiceProfile)
        }
        return binding.root

    }

    fun updateList(newList: List<BarbershopService>) {
        services = newList
        notifyDataSetChanged()
    }
}