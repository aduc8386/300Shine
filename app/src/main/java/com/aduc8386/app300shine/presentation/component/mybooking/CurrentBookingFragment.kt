package com.aduc8386.app300shine.presentation.component.mybooking

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.aduc8386.app300shine.databinding.FragmentListBookingBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.CustomerState
import com.aduc8386.app300shine.presentation.component.MainViewModel
import com.aduc8386.app300shine.presentation.component.home.HomeFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class CurrentBookingFragment :
    BaseFragment<FragmentListBookingBinding>(FragmentListBookingBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var currentBookingAdapter: CurrentBookingAdapter

    override fun bindView() {
        mainViewModel.getCurrentCustomer()
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            mainViewModel.currentCustomer.collectLatest {
                handleBarbershopsResponse(it)
            }
        }
    }

    override fun onClick(v: View?) {

    }

    private fun handleBarbershopsResponse(customerState: CustomerState) {
        if (customerState is CustomerState.LoggedIn) {
            with(binding) {
                rcvBooking.run {
                    currentBookingAdapter.apply {
                        updateList(customerState.data!!.bookings.filter {
                            it.status == "In process"
                        })
                        setOnItemClickListener {
                            findNavController().navigate(
                                HomeFragmentDirections.actionGlobalBarbershopFragment(
                                    it.id
                                )
                            )
                        }
                    }
                    layoutManager = LinearLayoutManager(requireContext())
                    adapter = currentBookingAdapter
                }
            }
        }
    }

}