package com.aduc8386.app300shine.presentation.component.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.domain.repo.BarbershopRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val barbershopRepository: BarbershopRepository
) : ViewModel() {

    private var _barbershops = MutableSharedFlow<List<Barbershop>>()
    val barbershops get() = _barbershops.asSharedFlow()

    fun getBarbershops() {
        viewModelScope.launch {
            barbershopRepository.getBarbershops().collectLatest {
                _barbershops.emit(it)
            }
        }
    }

}