package com.aduc8386.app300shine.presentation.component.booking

import android.content.Context
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.data.model.BarbershopService
import com.aduc8386.app300shine.data.model.ServiceByBarber
import com.aduc8386.app300shine.databinding.FragmentBookingBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.MainViewModel
import com.aduc8386.app300shine.presentation.component.barbershop.BarbershopViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.text.DecimalFormat
import javax.inject.Inject

@AndroidEntryPoint
class BookingFragment : BaseFragment<FragmentBookingBinding>(FragmentBookingBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()
    private val barbershopViewModel: BarbershopViewModel by viewModels()

    private val args: BookingFragmentArgs by navArgs()

    @Inject
    lateinit var barbershopServiceSpinnerAdapter: BarbershopServiceSpinnerAdapter

    @Inject
    lateinit var barbershopBarberSpinnerAdapter: BarbershopBarberSpinnerAdapter

    @Inject
    lateinit var barbershopDateSpinnerAdapter: BarbershopDateSpinnerAdapter

    @Inject
    lateinit var bookingTimeAdapter: BookingTimeAdapter

    private var barbershopId = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)
        barbershopId = args.barbershopId
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            launch {
                barbershopViewModel.barbershop.collectLatest {
                    bindBarbershopServices(it)
                }
            }
            launch {
                barbershopViewModel.selectedService.collectLatest {
                    bindService(it)
                }
            }
            launch {
                barbershopViewModel.selectedServiceByBarber.collectLatest {
                    bindServiceByBarber(it)
                }
            }
            launch {
                barbershopViewModel.selectedDate.collectLatest {
                    bindDate(it)
                }
            }
        }
    }



    override fun bindView() {

        barbershopViewModel.getBarbershopInformation(barbershopId)

        with(binding) {
            btnBookingBook.setOnClickListener(this@BookingFragment)
            spnBookingService.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    barbershopViewModel.setSelectedService(
                        barbershopViewModel.barbershop.value?.data?.services?.get(
                            position
                        )
                    )
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    barbershopViewModel.setSelectedService(
                        barbershopViewModel.barbershop.value?.data?.services?.get(
                            0
                        )
                    )
                }
            }
            spnBookingBarber.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    barbershopViewModel.setSelectedServiceByBarber(
                        barbershopViewModel.selectedService.value?.serviceByBarbers?.get(position)
                    )
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    barbershopViewModel.setSelectedServiceByBarber(
                        barbershopViewModel.selectedService.value?.serviceByBarbers?.get(0)
                    )
                }
            }
            spnBookingDate.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    barbershopViewModel.setSelectedDate(barbershopViewModel.dates[position])
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    barbershopViewModel.setSelectedDate(barbershopViewModel.dates[0])
                }
            }
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                btnBookingBook -> {
                    mainViewModel.book(barbershopViewModel.booking.value)
                }
            }
        }
    }

    private fun bindBarbershopServices(barbershopResponseStatus: ResponseStatus<Barbershop>?) {
        when (barbershopResponseStatus) {
            is ResponseStatus.Success -> {
                with(binding) {
                    spnBookingService.run {
                        adapter = barbershopServiceSpinnerAdapter.apply {
                            updateList(barbershopResponseStatus.data!!.services)
                        }
                    }
                }
            }

            else -> {}
        }
    }

    private fun bindService(barbershopService: BarbershopService?) {
        barbershopService?.let {
            with(binding) {
                spnBookingBarber.run {
                    adapter = barbershopBarberSpinnerAdapter.apply {
                        updateList(it.serviceByBarbers)
                    }
                }
                tvBookingPrice.text = "${DecimalFormat("#,###").format(barbershopService.price)}đ"
            }
        }
    }

    private fun bindServiceByBarber(serviceByBarber: ServiceByBarber?) {
        serviceByBarber?.let {
            binding.spnBookingDate.run {
                adapter = barbershopDateSpinnerAdapter.apply {
                    updateList(barbershopViewModel.dates)
                }
            }
        }
    }

    private fun bindDate(date: String?) {
        date?.let {
            binding.rcvBookingTime.run {
                adapter = bookingTimeAdapter.apply {
                    layoutManager = GridLayoutManager(requireContext(), 5)
                    setList(resources.getStringArray(R.array.times).toList())
                    setOnItemClickListener {
                        barbershopViewModel.setSelectedTime(it)
                    }
                }
            }

        }
    }
}