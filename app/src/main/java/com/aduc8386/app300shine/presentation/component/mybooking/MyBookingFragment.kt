package com.aduc8386.app300shine.presentation.component.mybooking

import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.databinding.FragmentMyBookingBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator

class MyBookingFragment :
    BaseFragment<FragmentMyBookingBinding>(FragmentMyBookingBinding::inflate) {

    override fun bindView() {
        setViewPagerAdapter()
    }

    private fun setViewPagerAdapter() {
        binding.vpViewpager.apply {
            adapter = MyBookingPagerAdapter(this@MyBookingFragment)
        }

        TabLayoutMediator(binding.tlTabLayout, binding.vpViewpager) { tab, position ->
            tab.text = listOf("Current", "History")[position]
        }.attach()

        for (i in 0..1) {
            val textView = LayoutInflater.from(requireContext())
                .inflate(R.layout.tab_layout_title, null) as TextView

            binding.tlTabLayout.getTabAt(i)?.customView = textView
        }

        Log.d("ducna", "setViewPagerAdapter: called")
    }

}