package com.aduc8386.app300shine.presentation.component.booking

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.BarbershopBarber
import com.aduc8386.app300shine.data.model.ServiceByBarber
import com.aduc8386.app300shine.databinding.ItemSpinnerBarberBinding
import com.aduc8386.app300shine.databinding.ItemSpinnerServiceBinding
import com.bumptech.glide.Glide

class BarbershopBarberSpinnerAdapter : BaseAdapter() {
    //    override fun bindViewHolder(binding: ItemSpinnerServiceBinding, item: BarbershopService) {
//        with(binding) {
//            val duration = String.format("%.2f minutes", item.duration)
//            tvSpinnerServiceName.text = item.name
//            tvSpinnerServiceDuration.text = duration
//
//        }
//    }
//
//    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemSpinnerServiceBinding
//        get() = { layoutInflater, viewGroup, _ ->
//            ItemSpinnerServiceBinding.inflate(layoutInflater, viewGroup, false)
//        }

    private var barbers: List<ServiceByBarber> = listOf()

    override fun getCount(): Int = barbers.size

    override fun getItem(position: Int): Any = barbers[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val binding =
            ItemSpinnerBarberBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        val barber = barbers[position]

        with(binding) {
            tvSpinnerBarberName.text = barber.barber?.name ?: ""
            tvSpinnerBarberType.text = "Classic Barber"
            Glide.with(root)
                .load(barber.barber?.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivSpinnerBarberProfile)
        }
        return binding.root

    }

    fun updateList(newList: List<ServiceByBarber>) {
        barbers = newList
        notifyDataSetChanged()
    }
}