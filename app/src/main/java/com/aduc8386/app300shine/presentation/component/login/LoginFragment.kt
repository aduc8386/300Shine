package com.aduc8386.app300shine.presentation.component.login

import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aduc8386.app300shine.databinding.FragmentLoginBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.CustomerState
import com.aduc8386.app300shine.presentation.component.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>(FragmentLoginBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModels() {
        lifecycleScope.launch {
            mainViewModel.currentCustomer.collectLatest {
                handleCustomerState(it)
            }
        }
    }

    private fun handleCustomerState(customerState: CustomerState) {
        when (customerState) {
            is CustomerState.LoggedIn -> {
                findNavController().navigate(LoginFragmentDirections.actionGlobalHomeFragment())
            }

            else -> {}
        }
    }

    override fun bindView() {
        with(binding) {
            tvLoginRegister.setOnClickListener {
                findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
            }

            btnLoginLogin.setOnClickListener {
                mainViewModel.login(
                    edtLoginEmail.text.toString().trim(),
                    edtLoginPassword.text.toString().trim()
                )
            }
        }
    }
}