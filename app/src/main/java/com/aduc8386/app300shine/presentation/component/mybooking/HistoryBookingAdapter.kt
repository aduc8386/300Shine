package com.aduc8386.app300shine.presentation.component.mybooking

import android.view.LayoutInflater
import android.view.ViewGroup
import com.aduc8386.app300shine.R
import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.databinding.ItemCurrentBookingBinding
import com.aduc8386.app300shine.databinding.ItemHistoryBookingBinding
import com.aduc8386.app300shine.presentation.base.BaseAdapter
import com.bumptech.glide.Glide
import java.text.DecimalFormat

class HistoryBookingAdapter : BaseAdapter<ItemHistoryBookingBinding, Booking>() {
    override fun bindViewHolder(binding: ItemHistoryBookingBinding, item: Booking) {
        with(binding) {
            Glide.with(root)
                .load(item.barbershop!!.profile)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemHistoryBookingBarbershopProfile)

            Glide.with(root)
                .load(item.barbershop.banner)
                .centerCrop()
                .error(R.drawable.ic_about_us)
                .into(rivItemHistoryBookingBarbershopBanner)

            tvItemHistoryBookingTotalBill.text = "${ DecimalFormat("#,###").format(item.service!!.price)}đ"
            tvItemHistoryBookingServiceName.text = item.service.name
        }
    }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Int) -> ItemHistoryBookingBinding
        get() = { layoutInflater, viewGroup, _ ->
            ItemHistoryBookingBinding.inflate(layoutInflater, viewGroup, false)
        }
}