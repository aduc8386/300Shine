package com.aduc8386.app300shine.presentation.component.barbershop.service

import android.content.Context
import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import com.aduc8386.app300shine.databinding.FragmentBarbershopServiceBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.barbershop.BarbershopViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class BarbershopServiceFragment :
    BaseFragment<FragmentBarbershopServiceBinding>(FragmentBarbershopServiceBinding::inflate) {

    private val barbershopViewModel: BarbershopViewModel by viewModels()

    @Inject
    lateinit var barbershopServiceAdapter: BarbershopServiceAdapter

    private var barbershopId: String = ""

    override fun onAttach(context: Context) {
        super.onAttach(context)

        barbershopId = requireArguments().getString("BARBERSHOP_ID", "null")
        Log.d("ducna", "onAttach: $barbershopId")
    }

    override fun bindView() {
        barbershopViewModel.getBarbershopInformation(barbershopId)
    }

    override fun observeViewModels() {
        lifecycleScope.launch {
            barbershopViewModel.barbershop.collectLatest {
                handleBarbershopInformation(it)
            }
        }
    }

    private fun handleBarbershopInformation(barbershopResponseStatus: ResponseStatus<Barbershop>?) {
        when (barbershopResponseStatus) {
            is ResponseStatus.Success -> {
                with(binding) {
                    barbershopServiceAdapter.apply {
                        updateList(barbershopResponseStatus.data!!.services)
                    }
                    rcvServiceServices.run {
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = barbershopServiceAdapter
                    }
                }
            }

            is ResponseStatus.Error -> {

            }

            is ResponseStatus.Loading -> {

            }

            else -> {

            }
        }
    }

}