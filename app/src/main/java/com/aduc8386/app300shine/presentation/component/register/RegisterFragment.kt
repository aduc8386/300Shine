package com.aduc8386.app300shine.presentation.component.register

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.aduc8386.app300shine.databinding.FragmentRegisterBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment
import com.aduc8386.app300shine.presentation.component.CustomerState
import com.aduc8386.app300shine.presentation.component.MainViewModel
import com.aduc8386.app300shine.presentation.component.login.LoginFragmentDirections
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {

    private val mainViewModel: MainViewModel by activityViewModels()

    override fun observeViewModels() {
        lifecycleScope.launch {
            mainViewModel.currentCustomer.collectLatest {
                handleCustomerState(it)
            }
        }
    }

    private fun handleCustomerState(customerState: CustomerState) {
        when (customerState) {
            is CustomerState.Registered -> {
                findNavController().navigate(LoginFragmentDirections.actionGlobalLoginFragment())
            }

            else -> {}
        }
    }

    override fun bindView() {
        with(binding) {
            btnRegisterRegister.setOnClickListener(this@RegisterFragment)
            tvRegisterLogin.setOnClickListener(this@RegisterFragment)
        }
    }

    override fun onClick(v: View?) {
        with(binding) {
            when (v) {
                btnRegisterRegister -> {
                    mainViewModel.register(
                        email = edtRegisterEmail.text.toString().trim(),
                        password = edtRegisterPassword.text.toString().trim(),
                        name = edtRegisterFullName.text.toString().trim(),
                        phoneNumber = edtRegisterPhoneNumber.text.toString().trim(),
                    )
                }
                tvRegisterLogin -> {
                    findNavController().navigate(RegisterFragmentDirections.actionGlobalLoginFragment())
                }
            }
        }
    }
}