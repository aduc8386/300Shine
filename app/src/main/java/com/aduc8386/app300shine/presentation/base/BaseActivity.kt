package com.aduc8386.app300shine.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.viewbinding.ViewBinding
import com.aduc8386.app300shine.R
import java.lang.IllegalArgumentException

abstract class BaseActivity<VB : ViewBinding>(private val bindingInflater: (inflater: LayoutInflater) -> VB) :
    AppCompatActivity() {

    protected lateinit var navController: NavController
    private var _binding: VB? = null
    val binding: VB
        get() = _binding!!


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = bindingInflater(layoutInflater)
        if (_binding == null) throw IllegalArgumentException("Binding cannot be null")
        navController =
            (supportFragmentManager.findFragmentById(R.id.fcv_main_container) as NavHostFragment).navController
        setContentView(binding.root)
        bindView()
    }

    abstract fun bindView()

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}