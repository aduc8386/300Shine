package com.aduc8386.app300shine.presentation.component.onboarding

import androidx.navigation.fragment.findNavController
import com.aduc8386.app300shine.databinding.FragmentOnboardingBinding
import com.aduc8386.app300shine.presentation.base.BaseFragment

class OnBoardingFragment :
    BaseFragment<FragmentOnboardingBinding>(FragmentOnboardingBinding::inflate) {
    override fun bindView() {
        with(binding) {
            btnOnboardingGetStarted.setOnClickListener {
                findNavController()
                    .navigate(OnBoardingFragmentDirections.actionGlobalLoginFragment())
            }
        }
    }
}