package com.aduc8386.app300shine.domain.repo

import com.aduc8386.app300shine.data.model.Booking
import com.aduc8386.app300shine.data.model.Customer
import com.aduc8386.app300shine.presentation.component.CustomerState
import kotlinx.coroutines.flow.Flow

interface CustomerRepository {

    suspend fun insertCustomerFullInformation(customer: Customer): CustomerState

    suspend fun insertCustomerEmailNamePhonePassword(
        customer: Customer
    ): CustomerState

    suspend fun getCustomerByEmailAndPassword(email: String, password: String): CustomerState

    suspend fun getCurrentCustomer(): Flow<CustomerState>

    fun insertBooking(booking: Booking): Boolean
}