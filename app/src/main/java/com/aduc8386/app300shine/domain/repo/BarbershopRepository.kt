package com.aduc8386.app300shine.domain.repo

import com.aduc8386.app300shine.data.ResponseStatus
import com.aduc8386.app300shine.data.model.Barbershop
import kotlinx.coroutines.flow.Flow

interface BarbershopRepository {

    suspend fun getBarbershops(): Flow<List<Barbershop>>

    suspend fun getBarbershopById(id: String): ResponseStatus<Barbershop>

    suspend fun getBarbershopsByName(query: String): Flow<List<Barbershop>>

}